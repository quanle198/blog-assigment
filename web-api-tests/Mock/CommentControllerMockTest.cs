﻿using System.Collections.Generic;
using Blog.Controllers;
using Blog.EF;
using Blog.Interface;
using Blog.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
namespace web_api_tests.Mock
{
    public class CommentControllerMockTest
    {
        CommentController _controller;
        Mock<IComment> moq;
        readonly List<Comment> lstitem1 = null;
        List<Comment> lstitem = new List<Comment>()
        {
            new Comment()
                {
                    CommentId =1 ,
                    Content="quanle",
                    PostId =1
                },
                new Comment()
                {
                    CommentId =2 ,
                    Content="quanlehoang",
                    PostId =1
                },
                new Comment()
                {
                    CommentId =3 ,
                    Content="heybaby",
                    PostId =2

                },
                new Comment()
                {
                    CommentId =4 ,
                    Content="heygirl",
                    PostId =2
                }
        };
        // Test to return null
        PagingViewModel paging = null;
        // List has value 
        List<PagingViewModel> lstpaging = new List<PagingViewModel>()
        {
            new PagingViewModel()
                {
                    CurrentPage=1,
                    Data =  new List<CreateViewModel>()
                    {
                       new CreateViewModel() { Content= "aaaaa", ID =1},
                       new CreateViewModel() { Content= "bbbbb", ID= 1}
                    }
                },
                new PagingViewModel()
                {
                    CurrentPage=2,
                     Data =  new List<CreateViewModel>()
                    {
                       new CreateViewModel() { Content= "aaaaa"},
                       new CreateViewModel() { Content= "bbbbb"}
                    }
                },
                new PagingViewModel()
                {
                    CurrentPage=3,
                    TotalItem=0,
                    TotalPage= 4,
                    Data =  new List<CreateViewModel>()
                    {
                       new CreateViewModel() { Content= "aaaaa"},
                       new CreateViewModel() { Content= "bbbbb"}
                    }
                }
        };

        public CommentControllerMockTest()
        {
            moq = new Mock<IComment>();
            _controller = new CommentController(moq.Object);
        }
        #region Test GetComments Method       
        [Fact]
        public void GetComments_WhenCalled_ReturnsOkResult()
        {
            // arrange            
            moq.Setup(x => x.GetComments()).Returns(lstitem);

            // Act
            var okResult = _controller.GetComments();

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);

        }

        [Fact]
        public void GetComments_WhenCalled_ReturnsNull()
        {
            // arrange            
            moq.Setup(x => x.GetComments()).Returns(lstitem1);

            // Act
            var nullResult = _controller.GetComments();

            // Assert
            Assert.IsType<NoContentResult>(nullResult.Result);
        }

        [Fact]
        public void GetComments_WhenCalled_ReturnsAllItems()
        {
            // arrange   
            moq.Setup(x => x.GetComments()).Returns(lstitem);

            // Act          
            var okResult = _controller.GetComments().Result as OkObjectResult;

            // Assert
            var items = Assert.IsType<List<Comment>>(okResult.Value);
            Assert.Equal(4, items.Count);
        }
        #endregion

        #region Test GetCommentPerPage Method
        [Fact]
        public void GetCommentPerPage_IdPassed_ReturnOkResult()
        {
            // Arrange
            var id = 1;
            var postid = 1;
            moq.Setup(x => x.GetCommentPerPage(id,postid)).Returns(lstpaging[0]);

            // Act
            var okResult = _controller.GetCommentPerPage(id,postid);

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetCommentPerPage_IdPassed_ReturnTotalPage()
        {
            // Arrange
            var id = 3;
            var postid = 1;
            moq.Setup(x => x.GetCommentPerPage(id, postid)).Returns(lstpaging[2]);

            // Act
            var result = _controller.GetCommentPerPage(id, postid).Result as OkObjectResult;

            // Assert
           Assert.Equal(4, (result.Value as PagingViewModel).TotalPage);
           //moq.Verify(x => x.GetCommentPerPage(id, postid), Times.Exactly(1));
        }
        #endregion

        #region Test PostComment Method
        [Fact]
        public void PostComment_InvalidObjectPassed_ReturnsBadRequest()
        {
            // Arrange
            var UserMissing = new Comment()
            {               
                Content = "Quan ne!!!"
            };
            // Act
            var badResponse = _controller.PostComment(UserMissing).Result.Result as BadRequestResult;

            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public void PostComment_ValidObjectPassed_ReturnsCreatedResponse()
        {
            // Arrange
            var ValidItem = new Comment()
            {
                UserId=1,
                Content = "Quan ne!!!"
            };

            // Act
            var createdResponse = _controller.PostComment(ValidItem).Result.Result as ObjectResult;

            // Assert
            Assert.IsType<CreatedAtActionResult>(createdResponse);
        }

        [Fact]
        public void PostComment_ValidObjectPassed_ReturnedResponseHasCreatedItem()
        {
            // Arrange
            var testItem = new Comment()
            {
                UserId=2,
                Content = "Quan ne!!!"
            };

            // Act
            var createdResponse = _controller.PostComment(testItem).Result.Result as CreatedAtActionResult;
            var item = createdResponse.Value as Comment;

            // Assert           
            Assert.IsType<Comment>(item);
            Assert.Equal("Quan ne!!!", item.Content);
        }
        #endregion
    }
}
