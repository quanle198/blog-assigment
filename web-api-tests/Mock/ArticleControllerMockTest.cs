﻿using Blog.Controllers;
using Blog.EF;
using Blog.Interface;
using Blog.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace web_api_tests.Mock
{
    public class ArticleControllerMockTest
    {
        ArticleController _controller;
        Mock<IPost> moq;

        List<CreateViewModel> lstitem = new List<CreateViewModel>()
        {
            new CreateViewModel()
                {
                    ID =1 ,
                    Title="quanle",
                    Intro = "hello world",
                    UserId =1
                },
                new CreateViewModel()
                {
                    ID =2 ,
                    Title="a",
                    Intro = "b",
                    UserId =1
                },
                new CreateViewModel()
                {
                    ID =1 ,
                    Title="quan",
                    Intro = "wow",
                    UserId =2

                },
                new CreateViewModel()
                {
                    ID =2 ,
                    Title="c",
                    Intro = "d",
                    UserId =2
                }

        };

        List<PagingViewModel> lstpaging = new List<PagingViewModel>()
        {
            new PagingViewModel()
                {
                    CurrentPage=1,
                    Data =  new List<CreateViewModel>()
                    {
                       new CreateViewModel() { Content= "aaaaa"},
                       new CreateViewModel() { Content= "bbbbb"}
                    }
                },
                new PagingViewModel()
                {
                    CurrentPage=2,
                     Data =  new List<CreateViewModel>()
                    {
                       new CreateViewModel() { Content= "aaaaa"},
                       new CreateViewModel() { Content= "bbbbb"}
                    },
                     TotalPage= 4
                },
                new PagingViewModel()
                {
                    CurrentPage=3,
                    TotalItem=0,
                    TotalPage= 4
                }
        };
        readonly List<CreateViewModel> lstitem1 = null;

        public ArticleControllerMockTest()
        {
            moq = new Mock<IPost>();
            _controller = new ArticleController(moq.Object);
        }

        public IFormFile File()
        {
            var fileMock = new Mock<IFormFile>();
            //Setup mock file using a memory stream
            var content = "Hello World from a Fake File";
            var fileName = "download.jpg";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);

            var file = fileMock.Object;
            return file;
        }

        #region Test GetArticleItems Method       
        [Fact]
        public void GetArticleItems_WhenCalled_ReturnsOkResult()
        {
            // arrange            
            moq.Setup(x => x.GetArticleItems()).Returns(lstitem);

            // Act
            var okResult = _controller.GetArticleItems();

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);

        }

        [Fact]
        public void GetArticleItems_WhenCalled_ReturnsNull()
        {
            // arrange            
            moq.Setup(x => x.GetArticleItems()).Returns(lstitem1);

            // Act
            var nullResult = _controller.GetArticleItems();

            // Assert
            Assert.IsType<NoContentResult>(nullResult.Result);
        }

        [Fact]
        public void GetArticleItems_WhenCalled_ReturnsAllItems()
        {
            // arrange   
            moq.Setup(x => x.GetArticleItems()).Returns(lstitem);

            // Act          
            var okResult = _controller.GetArticleItems().Result as OkObjectResult;

            // Assert
            var items = Assert.IsType<List<CreateViewModel>>(okResult.Value);
            Assert.Equal(4, items.Count);
        }
        #endregion

        #region Test GetArticleItem Method
        [Fact]
        public void GetArticleItem_IdPassed_ReturnsNotFoundResult()
        {
            // Arrange
            var id = 3;

            moq.Setup(x => x.GetArticleItem(id)).Returns(lstitem.Find(a => a.ID == id));

            // Act
            var notFoundResult = _controller.GetArticleItem(id);

            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult.Result);
        }

        [Fact]
        public void GetArticleItem_ExistingId_ReturnsOkResult()
        {
            // Arrange
            var id = 1;
            moq.Setup(x => x.GetArticleItem(id)).Returns(lstitem.Find(a => a.ID == id));

            // Act
            var okResult = _controller.GetArticleItem(id);

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetArticleItem_ExistingIdPassed_ReturnsRightItem()
        {
            // Arrange
            var id = 2;
            moq.Setup(x => x.GetArticleItem(id)).Returns(lstitem.Find(a => a.ID == id));

            // Act
            var okResult = _controller.GetArticleItem(id).Result as OkObjectResult;

            // Assert
            Assert.IsType<CreateViewModel>(okResult.Value);
            Assert.Equal(id, (okResult.Value as CreateViewModel).ID);
        }
        #endregion

        #region Test GetArticlePerPage Method
        [Fact]
        public void GetArticlePerPage_IdPassed_ReturnOkResult()
        {
            // Arrange
            var id = 2;
            moq.Setup(x => x.GetArticlePerPage(id)).Returns(lstpaging.Find(a => a.CurrentPage == id));

            // Act
            var okResult = _controller.GetArticlePerPage(id).Result ;

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void GetArticlePerPage_IdPassed_ReturnTotalPage()
        {
            // Arrange
            var id = 2;
            moq.Setup(x => x.GetArticlePerPage(id)).Returns(lstpaging.Find(a => a.CurrentPage == id));
            
            // Act
            var result = _controller.GetArticlePerPage(id).Result as OkObjectResult;

            // Assert
            Assert.Equal(4, (result.Value as PagingViewModel).TotalPage);
        }
        #endregion

        #region Test PostArticleItem Method
        [Fact]
        public void PostArticleItem_InvalidObjectPassed_ReturnsBadRequest()
        {
            // Arrange
            var nameMissingItem = new Post()
            {               
                Intro = "Hello",
                Content = "Quan ne!!!"
            };
            IFormFile file = File();
            // Act
            var badResponse = _controller.PostArticleItem(nameMissingItem, file).Result.Result as ObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(badResponse);
        }

        [Fact]
        public void PostArticleItem_ValidObjectPassed_ReturnsCreatedResponse()
        {
            // Arrange
            Post testItem = new Post()
            {
                Title ="Welcome......",
                Intro = "Hello",
                Content = "Quan ne!!!"
            };
            IFormFile file = File();

            // Act
            var createdResponse = _controller.PostArticleItem(testItem,file).Result.Result as ObjectResult;

            // Assert
            Assert.IsType<CreatedAtActionResult>(createdResponse);
        }

        [Fact]
        public void PostArticleItem_ValidObjectPassed_ReturnedResponseHasCreatedItem()
        {
            // Arrange
            var testItem = new Post()
            {
                Title = "Welcome......",
                Intro = "Hello baby",
                Content = "Quan ne!!!"
            };
            IFormFile file = File();

            // Act
            var createdResponse = _controller.PostArticleItem(testItem, file).Result.Result as CreatedAtActionResult;
            var item = createdResponse.Value as Post;

            // Assert           
            Assert.IsType<Post>(item);
            Assert.Equal("Hello baby", item.Intro);
        }
        #endregion

        #region Test GetMyBlog Method
        [Fact]
        public void GetMyBlog_IdPassed_ReturnsNotFoundResult()
        {
            // Arrange
            var userid = 3;
            moq.Setup(x => x.GetMyBlog(userid)).Returns(Task.FromResult(lstitem.FindAll(x => x.UserId == userid)));
            
            // Act
            var notFoundResult = _controller.GetMyBlog(userid);
            var result = notFoundResult.Result.Result;

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GetMyBlog_ExistingId_ReturnsOkResult()
        {
            // Arrange
            var userid = 1;
            moq.Setup(x => x.GetMyBlog(userid)).Returns(Task.FromResult(lstitem.FindAll(x => x.UserId == userid)));

            // Act
            var okResult = _controller.GetMyBlog(userid);

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result.Result);
        }

        [Fact]
        public void GetMyBlog_ExistingIdPassed_ReturnsRightItem()
        {
            // Arrange
            var userid = 2;
            moq.Setup(x => x.GetMyBlog(userid)).Returns(Task.FromResult(lstitem.FindAll(x => x.UserId == userid)));

            // Act
            var okResult = _controller.GetMyBlog(userid).Result.Result as ObjectResult;
            var items = Assert.IsType<List<CreateViewModel>>(okResult.Value);
            // Assert
            Assert.Equal(2, items.Count);
        }
        #endregion

        #region Test DeleteArticleItem Method
        [Fact]
        public void DeleteArticleItem_IdPassed_ReturnOkResult()
        {
            // Arrange
            var id = 1;
            moq.Setup(x => x.DeletePost(id)).Returns(1);

            // Act
            var okResult = _controller.DeleteArticleItem(id).Result;

            // Assert
            Assert.IsType<OkResult>(okResult);
        }

        [Fact]
        public void DeleteArticleItem_IdPassed_ReturnBadResult()
        {
            // Arrange
            int id = 12;
            moq.Setup(x => x.DeletePost(id)).Returns(0);

            // Act
            var BadResult = _controller.DeleteArticleItem(id).Result;

            // Assert
            Assert.IsType<BadRequestResult>(BadResult);
        }

        #endregion
    }
}
