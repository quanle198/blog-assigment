﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Blog.Controllers;
using Blog.EF;
using Blog.Interface;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace web_api_tests.Mock
{
    public class UsersControllerMockTest
    {
        UsersController _controller;
        Mock<IUser> moq;
        readonly List<User> lstitem1 = null;
        List<User> lstitem = new List<User>()
        {
            new User()
                {
                    UserId =1 ,
                    UserName="admin",
                    Role ="Administrator"
                },
            new User()
            {
                UserId =2 ,
                UserName="quanle",
                Role ="User"
            }

        };

        public UsersControllerMockTest()
        {
            moq = new Mock<IUser>();
            _controller = new UsersController(moq.Object);
        }
        #region Test GetUsers Method       
        [Fact]
        public void GetUsers_WhenCalled_ReturnsOkResult()
        {
            // arrange            
            moq.Setup(x => x.GetUsers()).Returns(lstitem);

            // Act
            var okResult = _controller.GetUsers();

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);

        }

        [Fact]
        public void GetUsers_WhenCalled_ReturnsNull()
        {
            // arrange            
            moq.Setup(x => x.GetUsers()).Returns(lstitem1);

            // Act
            var nullResult = _controller.GetUsers();

            // Assert
            Assert.IsType<NoContentResult>(nullResult.Result);
        }

        [Fact]
        public void GetUsers_WhenCalled_ReturnsAllItems()
        {
            // arrange   
            moq.Setup(x => x.GetUsers()).Returns(lstitem);

            // Act          
            var okResult = _controller.GetUsers().Result as OkObjectResult;

            // Assert
            var items = Assert.IsType<List<User>>(okResult.Value);
            Assert.Equal(2, items.Count);
        }
        #endregion
    }
}
