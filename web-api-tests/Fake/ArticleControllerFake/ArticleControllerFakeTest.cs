using Blog.Controllers;
using Blog.Interface;
using Blog.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Xunit;

namespace web_api_tests.Fake.ArticleControllerFake
{
    public class ArticleControllerFakeTest
    {
        ArticleController _controller;
        readonly IPost _service;

        public ArticleControllerFakeTest()
        {
            _service = new PostFake();
            _controller = new ArticleController(_service);
        }

        [Fact]
        public void GetArticleItems_WhenCalled_ReturnsOkResult()
        {
            // Act
            var okResult = _controller.GetArticleItems();

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetArticleItems_WhenCalled_ReturnsAllItems()
        {
            // Act
            var okResult = _controller.GetArticleItems().Result as OkObjectResult;

            // Assert
            var items = Assert.IsType<List<CreateViewModel>>(okResult.Value);
            Assert.Equal(2, items.Count);
        }

        [Fact]
        public void GetArticleItem_IdPassed_ReturnsNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.GetArticleItem(3);

            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult.Result);
        }

        [Fact]
        public void GetArticleItem_ExistingId_ReturnsOkResult()
        {
            // Arrange
            var id = 1;

            // Act
            var okResult = _controller.GetArticleItem(id);

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetArticleItem_ExistingIdPassed_ReturnsRightItem()
        {
            // Arrange
            var id = 2;

            // Act
            var okResult = _controller.GetArticleItem(id).Result as OkObjectResult;

            // Assert
            Assert.IsType<CreateViewModel>(okResult.Value);
            Assert.Equal(id, (okResult.Value as CreateViewModel).ID);
        }

        [Fact]
        public void GetArticlePerPage_ValidIdPassed_ReturnBadResult()
        {
            // Arrange
            var id = 1;

            // Act
            var okResult = _controller.GetArticlePerPage(id).Result as OkObjectResult;

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void GetArticlePerPage_InValidIdPassed_ReturnBadResult()
        {
            // Arrange
            var id = 2;

            // Act
            var okResult = _controller.GetArticlePerPage(id).Result as NoContentResult;

            // Assert
            Assert.IsType<NoContentResult>(okResult);
        }
    }
}
