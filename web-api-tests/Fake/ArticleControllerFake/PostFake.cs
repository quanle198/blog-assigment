﻿using Blog.EF;
using Blog.Interface;
using Blog.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_api_tests.Fake.ArticleControllerFake
{
    class PostFake : IPost
    {
        private BlogDbContext GetContextWithData()
        {
            var options = new DbContextOptionsBuilder<BlogDbContext>()
                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                              .Options;
            var context = new BlogDbContext(options);

            List<Post> lstpost = new List<Post>()
            {
                new Post { PostId = 1, Intro = "Beers" , UserId= 1, Status=true},
                new Post { PostId = 2, Intro = "Wines" , UserId = 1 , Status = true }
            };
            foreach (var item in lstpost)
            {
                context.Posts.Add(item);
            }

            List<User> lstuser = new List<User>()
            {
                new User { UserId = 1 },
                new User { UserId = 2 }
            };
            foreach (var item in lstuser)
            {
                context.Users.Add(item);
            }

            context.SaveChanges();
            return context;
        }

        public PostFake()
        {

        }

        public IEnumerable<CreateViewModel> GetArticleItems()
        {
            var model = (from p in GetContextWithData().Posts
                         join u in GetContextWithData().Users on p.UserId equals u.UserId
                         where p.Status == true
                         select new CreateViewModel { ID = p.PostId, Title = p.Title, Intro = p.Intro, CreatedAt = p.CreatedAt, PathImage = p.ImagePath, Content = p.Content, UserId = u.UserId, Username = u.UserName })
                              .OrderByDescending(x => x.ID)
                              .ToList();
            return model;
        }

        public CreateViewModel GetArticleItem(int id)
        {
            var model = (from p in GetContextWithData().Posts
                         join u in GetContextWithData().Users on p.UserId equals u.UserId
                         where p.PostId == id && p.Status == true
                         select new CreateViewModel { ID = p.PostId, Title = p.Title, Intro = p.Intro, CreatedAt = p.CreatedAt, PathImage = p.ImagePath, Content = p.Content, Status = p.Status, Username = u.UserName, UserId = u.UserId })
                              .SingleOrDefault();
            return model;

        }

        public PagingViewModel GetArticlePerPage(int id)
        {
            int Per_page = 3;
            int TotalItem = ((from p in GetContextWithData().Posts where p.Status == true select p).Count());
            double num = TotalItem / (double)Per_page;
            int TotalPage = (int)Math.Ceiling(num);
            int Start = (id * Per_page - (Per_page - 1)) - 1;
            var model = (from p in GetContextWithData().Posts
                         join u in GetContextWithData().Users on p.UserId equals u.UserId
                         where p.Status == true
                         select new CreateViewModel { ID = p.PostId, Title = p.Title, Intro = p.Intro, CreatedAt = p.CreatedAt, PathImage = p.ImagePath, Content = p.Content, Username = u.UserName, Role = u.Role })
                              .OrderByDescending(x => x.ID).Skip(Start).Take(Per_page)
                              .ToList();
            PagingViewModel paging = new PagingViewModel()
            {
                CurrentPage = id,
                TotalPage = TotalPage,
                Per_page = Per_page,
                TotalItem = TotalItem,
                Data = model
            };
            return paging;
        }

        public void AddPost(Post item)
        {
            GetContextWithData().Posts.Add(item);
            GetContextWithData().SaveChanges();
        }
        public async Task<List<CreateViewModel>> GetMyBlog(int id)
        {
            var model = await (from p in GetContextWithData().Posts
                               join u in GetContextWithData().Users on p.UserId equals u.UserId
                               where u.UserId == id && p.Status == true
                               select new CreateViewModel { ID = p.PostId, Title = p.Title, Intro = p.Intro, CreatedAt = p.CreatedAt, PathImage = p.ImagePath, Content = p.Content, Username = u.UserName })
                              .ToListAsync();
            return model;
        }
        public async Task SaveChange()
        {
            await GetContextWithData().SaveChangesAsync();

        }
        public int DeletePost(int id)
        {
            Post item = new Post();
            item = (from p in GetContextWithData().Posts
                    where (p.PostId == id)
                    select p
                          ).SingleOrDefault();
            item.Status = false;
            if (item == null)
            { return 0; }
            GetContextWithData().Entry(item).State = EntityState.Modified;
            return 1;
        }
    }
}
