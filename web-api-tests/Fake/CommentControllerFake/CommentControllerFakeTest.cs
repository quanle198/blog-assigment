﻿using Blog.Controllers;
using Blog.Interface;
using Blog.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Xunit;

namespace web_api_tests.Fake.CommentControllerFake
{
    public class CommentControllerFakeTest
    {
        CommentController _controller;
        readonly IComment _service;

        public CommentControllerFakeTest()
        {
            _service = new CommentFake();
            _controller = new CommentController(_service);
        }

        [Fact]
        public void GetCommentPerPage_IdPassed_ReturnNoContentResult()
        {
            // Arrange
            var id = 1;
            var postid = 5;

            // Act
            var NoContentResult = _controller.GetCommentPerPage(id, postid);

            // Assert
            Assert.IsType<NoContentResult>(NoContentResult.Result);
        }
        [Fact]
        public void GetCommentPerPage_IdPassed_ReturnOkResultResult()
        {
            // Arrange
            var id = 1;
            var postid = 2;

            // Act
            var OkResult = _controller.GetCommentPerPage(id, postid) ;

            // Assert
            Assert.IsType<OkObjectResult>(OkResult.Result);
        }
    }
}
