﻿using Blog.EF;
using Blog.Interface;
using Blog.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_api_tests.Fake.CommentControllerFake
{
    class CommentFake : IComment
    {
        private BlogDbContext GetContextWithData()
        {
            var options = new DbContextOptionsBuilder<BlogDbContext>()
                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                              .Options;
            var context = new BlogDbContext(options);
            // Fake DB Record of Post table
            List<Post> lstpost = new List<Post>()
            {
                new Post { PostId = 1, Intro = "Beers" , UserId= 1, Status=true},
                new Post { PostId = 2, Intro = "Wines" , UserId = 1 , Status = true }
            };
            foreach (var item in lstpost)
            {
                context.Posts.Add(item);
            }
            // Fake DB Record of User table
            List<User> lstuser = new List<User>()
            {
                new User { UserId = 1 },
                new User { UserId = 2 }
            };
            foreach (var item in lstuser)
            {
                context.Users.Add(item);
            }
            // Fake DB Record of Comment table
            List<Comment> lstcmt = new List<Comment>()
            {
                new Comment { CommentId = 1 ,Content =" hay qua", PostId=1, UserId=1},
                new Comment { CommentId = 2, Content="tam tam" , PostId=2,UserId=2}
            };
            foreach (var item in lstcmt)
            {
                context.Comments.Add(item);
            }

            context.SaveChanges();
            return context;
        }

        public CommentFake()
        {

        }
        public IEnumerable<Comment> GetComments()
        {
            return GetContextWithData().Comments.ToList();
        }
        public PagingViewModel GetCommentPerPage(int id, int postid)
        {
            int Per_page = 5;
            int TotalItem = ((from c in GetContextWithData().Comments where c.PostId == postid select c).Count());
            double num = TotalItem / (double)Per_page;
            int TotalPage = (int)Math.Ceiling(num);
            int Start = (id * Per_page - (Per_page - 1)) - 1;
            var lstCmt = (from c in GetContextWithData().Comments
                          join u in GetContextWithData().Users on c.UserId equals u.UserId
                          join p in GetContextWithData().Posts on c.PostId equals p.PostId
                          where p.PostId == postid
                          select new CreateViewModel { ID = p.PostId, CmtId = c.CommentId, Title = p.Title, Intro = p.Intro, CreatedAt = p.CreatedAt, PathImage = p.ImagePath, Content = p.Content, UserId = u.UserId, Username = u.UserName, ContentCmt = c.Content, CreatedAtCmt = c.CreateAt })
                          .OrderByDescending(x => x.CmtId).Skip(Start).Take(Per_page)
                          .ToList();
            PagingViewModel paging = new PagingViewModel()
            {
                CurrentPage = id,
                TotalPage = TotalPage,
                Per_page = Per_page,
                TotalItem = TotalItem,
                Data = lstCmt
            };
            return paging;
        }
        public async Task PostComment(Comment comment)
        {
            GetContextWithData().Comments.Add(comment);
            await GetContextWithData().SaveChangesAsync();
        }
    }
}
