﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Blog.EF;
using Blog.Interface;
using Blog.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using TinifyAPI;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Blog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : Controller
    {
        private readonly IPost _post;
        public ArticleController(IPost post)
        {
            _post = post;
        }
        // GET: api/<controller>
        [HttpGet]
        public ActionResult<IEnumerable<CreateViewModel>> GetArticleItems()
        {
            var items = _post.GetArticleItems();
            if(items == null)
            {
                return NoContent();
            }
            return Ok(items);
        }

        // GET: api/<controller>/page/{i}
        [HttpGet]
        [Route("page/{id}")]
        public ActionResult<PagingViewModel> GetArticlePerPage(int id)
        {
            var items = _post.GetArticlePerPage(id);
            if (!items.Data.Any())
            {
                return NoContent();
            }
            return Ok(items);
        }

        // GET: api/Article/5
        [HttpGet("{id}")]
        public ActionResult<CreateViewModel> GetArticleItem(int id)
        {
            var item = _post.GetArticleItem(id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }
    
        // Resize Image
        private async void ResizeImage(string sFilePath, string uploadsFolder)
        {
            Tinify.Key = "ttnnMQRS423FHjVYlSQ5b9sZjfZQPzXV"; //TinyPNG Developer API KEY
            //sFilePath = /*@"d:\ButterFly.jpg";*/ /*Url.Content("~/img/" + sFilePath);*/ 
            Random rnd = new Random();
            int num = rnd.Next(1, 1000);
            string scaleFile = @uploadsFolder + "\\" + num + ".jpg";

            var source = Tinify.FromFile(sFilePath);
            var scaleresized = source.Resize(new
            {
                method = "scale",
                width = 600

            });

            await scaleresized.ToFile(scaleFile);

        }

        // GET: api/<controller>/Myblog/{id}
        [HttpGet]
        [Route("Myblog/{id}")]
        public async Task<ActionResult<IEnumerable<CreateViewModel>>> GetMyBlog(int id)
        {
            var item = await _post.GetMyBlog(id);
            if(item.Count == 0)
            {
                return NotFound();
            }
            return Ok(item);
        }

        // POST: api/Article
        [HttpPost]
        public async Task<ActionResult<Post>> PostArticleItem([FromForm]Post item, [FromForm]IFormFile body)
        {
            if (item.Title==null)
            {
                return BadRequest(ModelState);
            }
            if (body != null)
            {
                var filename = body.FileName;
                var contentType = body.ContentType;
                string uploadsFolder = Path.Combine("ClientApp\\src\\Assets\\img");
                string uploadsFolderResize = Path.Combine("ClientApp\\src\\Assets\\imgResize");
                string uniqueFileName = Guid.NewGuid().ToString() + "_" + body.FileName;

                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                string filePathResize = Path.Combine(uploadsFolder, body.FileName);

                await body.CopyToAsync(new FileStream(filePath, FileMode.Create));
                ResizeImage(filePathResize, uploadsFolderResize);
                item.ImagePath = uniqueFileName;
            }
            else
            {
                item.ImagePath = "empty.jpg";
            }
            item.CreatedAt = DateTime.Now;
            _post.AddPost(item);            
            return CreatedAtAction(nameof(GetArticleItem), new { id = item.PostId }, item);
        }


        // PUT: api/Article/5 Delete by update status to false
        [HttpPut("{id}")]
        public async Task<IActionResult> DeleteArticleItem(int id)
        {
            var result =_post.DeletePost(id);
            if(result == 0)
            {
                return BadRequest();
            }
            await _post.SaveChange();
            return Ok();
        }       
    }
}
