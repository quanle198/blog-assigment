﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Blog.EF;
using Blog.Interface;

namespace Blog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUser _user;

        public UsersController(IUser user)
        {
            _user = user;
        }

        // GET: api/Users
        [HttpGet]
        public  ActionResult<IEnumerable<User>> GetUsers()
        {
            var items = _user.GetUsers();
            if (items == null)
            {
                return NoContent();
            }
            return Ok(items);
        }

    }
}
