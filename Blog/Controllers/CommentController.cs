﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Blog.EF;
using Blog.ViewModel;
using Blog.Interface;

namespace Blog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly IComment _comment;

        public CommentController(IComment comment)
        {
            _comment = comment;
        }

        // GET: api/Comment
        [HttpGet]
        public ActionResult<IEnumerable<Comment>> GetComments()
        {
            var items = _comment.GetComments();
            if (items == null)
            {
                return NoContent();
            }
            return Ok(items);
        }

        // GET: api/Comment/page/{postid}/{id}
        [HttpGet]
        [Route("page/{postid}/{id}")]
        public ActionResult<PagingViewModel> GetCommentPerPage(int id, int postid)
        {
            var items = _comment.GetCommentPerPage(id, postid);
            if (!items.Data.Any())
            {
                return NoContent();
            }
            return Ok(items);
          
        }

        // POST: api/comment/
        [HttpPost]
        public async Task<ActionResult<Comment>> PostComment([FromForm] Comment comment)
        {
            comment.CreateAt = DateTime.Now;
            if(comment.Content == null || comment.UserId == 0)
            {
                return BadRequest();
            }
            await _comment.PostComment(comment);
            return CreatedAtAction("GetComment", new { id = comment.CommentId }, comment);
        }
    }
}
