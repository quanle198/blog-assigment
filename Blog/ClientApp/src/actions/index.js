﻿import axios from 'axios';

//Action for Home component
export function fetchBlogPerPage(pageNumber) {
    return dispatch => {
        fetch(`api/article/page/${pageNumber}`)
            .then(res => res.json())
            .then(res => {
                dispatch(getBlogPerPage(res))
            })
    };
}

export function getBlogPerPage(request) {
    console.log("getBlogPerPage", request);
    return {
        type: "getBlogPerPage",
        pageload: request,
    }
}

//Action for Login, Logout component
export function fetchUser() {
    return dispatch => {
        axios
            .get('api/users')
            .then(res => {
                dispatch(getUser(res.data));
            })
            .catch(error => console.log(error));
    };
}
export function getUser(request) {
    console.log("getUser", request);
    return {
        type: "getUser",
        pageload: request,
    }
}
export function hadLogin(status) {
    console.log("hadLogin");
    return {
        type: "hadLogin",
        status: status
    }
}

export function Userchange(text) {
    console.log("Userchange",text);
    return {
        type: "Userchange",
        text: text
    }
}
export function Passchange(textpass) {
    console.log("Passchange", textpass);
    return {
        type: "Passchange",
        textpass: textpass
    }
}

//Action for Detail

export function fetchDetailBlog(id) {
    return dispatch => {
        axios
            .get(`api/article/${id}`)
            .then(res => {
                dispatch(getDetailBlog(res.data));
            })
            .catch(error => console.log(error));
    };
}

export function getDetailBlog(request) {
    console.log("getDetailBlog", request);
    return {
        type: "getDetailBlog",
        pageload: request,
    }
}

export function fetchCommentPerPage(postid, pageNumber) {
    return dispatch => {
        axios
            .get(`api/comment/page/${postid}/${pageNumber}`)
            .then(res => {
                if (res.data) {
                    dispatch(getCommentPerPage(res.data))
                }
                else {
                    console.log("empty")
                    dispatch(changLoadingcmnt())
                }
            })
            .catch(error => {

                console.log(error)
            })
    };
}

export function changLoadingcmnt() {
    console.log("changLoadingcmnt");
    return {
        type: "changLoadingcmnt",        
    }
}

export function getCommentPerPage(request) {
    console.log("getCommentPerPage", request);
    return {
        type: "getCommentPerPage",
        pageload: request,
    }
}

export function postcmt(formData) {
    return (dispatch) => {
        axios({
            method: 'post',
            url: 'api/comment/',
            data: formData,
            config: { headers: { 'Content-Type': 'multipart/form-data' } }
        })
            .then(function (response) {
                //handle success
                dispatch(successPost())
                console.log("res", response);
            })
            .catch(function (response) {
                //handle error
                console.log("error", response);
            });
    };
}
export function successPost() {
    console.log("successPost");
    return {
        type: "successPost"
    }
}

//Action for Myblog Component
export function fetchMyblog(id) {
    return dispatch => {
        axios
            .get(`api/article/myblog/${id}`)
            .then(res => {
                dispatch(getMyblog(res.data));
            })
            .catch(error => console.log(error));
    };
}
export function getMyblog(request) {
    console.log("getMyblog", request);
    return {
        type: "getMyblog",
        pageload: request,
    }
}