import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch } from 'react-router-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from "react-redux"
import { store } from './store'

//const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');
//const rootElement = document.getElementById('root');

//const render = () => ReactDOM.render(
//    <Provider store={store} >
//        <BrowserRouter basename={baseUrl}>
//            <Switch>
//                <App />
//            </Switch>
//        </BrowserRouter>
//    </Provider>,
//    rootElement);

//store.subscribe(render);
//render();
ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <App />
            </Switch>
        </BrowserRouter>
    </Provider>
    , document.getElementById("root"));
registerServiceWorker();
