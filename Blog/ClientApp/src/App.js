import React, { Component } from 'react';
import { Switch, Route } from 'react-router';
import { Layout } from './components/Layout';
import Page from './components/Page';

export default class App extends Component {

    render() {
        console.log('app  component');
        return (
            <Layout>
                <Switch>
                    <Route path='/' component={Page} key='test'></Route>
                </Switch>
            </Layout>
        );
    }
}
