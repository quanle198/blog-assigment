﻿import { homeReducer } from "./homeReducer";
import { loginReducer } from "./loginReducer";
import { detailReducer } from "./DetailReducer";
import { myblogReducer } from "./myblogReducer";
import { combineReducers } from "redux";

export default combineReducers({
    homeReducer,
    loginReducer,
    detailReducer,
    myblogReducer
});