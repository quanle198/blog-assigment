﻿const initialState = {
    article: [],
    loading: true,

    comment: [],
    total: null,
    per_page: null,
    current_page: null,
    loadingcmt: true,
    flag: false,
    errorcmt: false
};
export const detailReducer = (state = initialState, action) => {
    console.log("action", action)
    switch (action.type) {
        case "getDetailBlog":
            return {
                ...state,
                article: action.pageload,
                loading: false
            };
        case "getCommentPerPage":
            return {
                ...state,
                comment: action.pageload.data,
                total: action.pageload.totalItem,
                per_page: action.pageload.per_page,
                current_page: action.pageload.currentPage,
                loadingcmt: false
            };
        case "successPost":
            return {
                ...state,               
                flag: true
            };
        case "changLoadingcmnt":
            return {
                ...state,
                loadingcmt: true
            };
        default:
            return state;
    }
};
