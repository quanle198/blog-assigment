﻿const initialState = {
    article: [],
    loading: true,
};
export const myblogReducer = (state = initialState, action) => {
    console.log("action", action)
    switch (action.type) {
        case "getMyblog":
            return {
                ...state,
                article: action.pageload,
                loading: false
            };
        default:
            return state;
    }
};
