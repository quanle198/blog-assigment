﻿const initialState = {
    article: [],
    total: null,
    per_page: null,
    current_page: null,
    loading: true,
    flag: false
};
export const homeReducer = (state = initialState, action) => {
    console.log("action", action)
    switch (action.type) {
        case "getBlogPerPage":
            return {
                ...state,
                article: action.pageload.data,
                total: action.pageload.totalItem,
                per_page: action.pageload.per_page,
                current_page: action.pageload.currentPage,
                loading: false,
                flag: false
            };
        default:
            return state;
    }
};
