﻿const initialState = {
    account: [],
    loggedIn: false,
    username: '',
    password:''
};
export const loginReducer = (state = initialState, action) => {
    console.log("action", action)
    switch (action.type) {
        case "getUser":
            return {
                ...state,
                account: action.pageload
            };
        case "hadLogin":
            return {
                ...state.loggedIn,
                loggedIn: action.status
            };
        case "Userchange":
            return {
                ...state,
                username: action.text
            };
        case "Passchange":
            return {
                ...state,
                password: action.textpass
            }

        default:
            return state;
    }
};
