﻿import { createStore, applyMiddleware, compose } from "redux";
import combineReducers from "../reducer/index";
import  thunk from 'redux-thunk';
import ReduxPromise from 'redux-promise';

export const store = createStore(combineReducers, compose(applyMiddleware(thunk, ReduxPromise)));