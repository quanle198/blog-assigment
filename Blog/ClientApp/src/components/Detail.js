﻿import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import { fetchDetailBlog, fetchCommentPerPage, postcmt } from "./../actions"
import { connect } from 'react-redux'

class Detail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            commenttext: '',
            flag: false
        };
        this.handleOnchange = this.handleOnchange.bind(this)
        this.submitForm = this.submitForm.bind(this)
    }

    handleOnchange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    makeHttpRequestWithPage = async pageNumber => {
        let postid = this.props.match.params.id;
        this.props.fetchCommentPerPageFunc(postid, pageNumber);
    }

    submitForm(event) {
        event.preventDefault()
        let postid = this.props.match.params.id;

        const formData = new FormData();
        formData.set('Content', this.state.commenttext);
        formData.set('PostId', postid);
        formData.set('UserId', localStorage.getItem("userid"));
        this.props.postcmtFunc(formData)
        window.location.reload()
        
    }

    componentDidMount() {
        console.log("accessDidmount")
        let id = this.props.match.params.id;
        this.props.fetchDetailBlogFunc(id)
        this.makeHttpRequestWithPage(1);
    }

    static renderArticlesDetail(article) {
        var options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };
        const date = new Date(article.createdAt).toLocaleDateString([], options);
        return (
            <React.Fragment key={article.id}>
                <div className="post-preview">
                    <h2 className="post-title">{article.title}</h2>
                    <img src={require(`../Assets/img/${article.pathImage}`)} alt="" width="300px" style={{ textAlign: "center" }} />
                    <h3 className="post-subtitle">
                        {article.intro}
                    </h3>
                    <p className="post-meta">
                        Posted by
                        <span style={{ "fontWeight": "bold" }}> {article.username} </span>
                        on
                                <span style={{ "fontWeight": "bold" }}> {date} </span>
                    </p>
                    <hr></hr>
                    <p dangerouslySetInnerHTML={{ __html: article.content }}>
                    </p>
                </div>
                <hr></hr>
            </React.Fragment>
        )
    }

    static renderCommentsDetail(comment) {
        return (
            comment.map((comments) => {
                return (
                    <React.Fragment key={comments.cmtId}>
                        <li className="media">
                            <div className="media-body">
                                <strong className="text-success">{comments.username}</strong>
                                <span className="text-muted pull-right">
                                    <small className="text-muted"> 1h ago</small>
                                </span>
                                <p>{comments.contentCmt}</p>                                                                                                                </div>
                            <div>
                                <a>Delete</a>
                            </div>
                        </li>
                        <hr></hr>
                    </React.Fragment>
                )
            })
        )
    }

    render() {     
        console.log("cmt", this.state.commenttext)
        if (this.props.detailReducer.flag) {
            alert("comment success");
        }
        //if (this.props.detailReducer.errorcmt) {
        //    return (
        //        <p><em>No comment</em></p>
        //    )
        //}
        let contents = this.props.detailReducer.loading
            ? <p><em>Loading...</em></p>
            : Detail.renderArticlesDetail(this.props.detailReducer.article);

        let commentform = this.props.detailReducer.loadingcmt 
            ? <p><em>Loading...</em></p>
            : Detail.renderCommentsDetail(this.props.detailReducer.comment)

        // Get number page
        const pageNumbers = [];
        if (this.props.detailReducer.total !== null) {
            for (let i = 1; i <= Math.ceil(this.props.detailReducer.total / this.props.detailReducer.per_page); i++) {
                pageNumbers.push(i);
            }
        }
        // Render Page Number
        let renderPageNumbers = pageNumbers.map(number => {
            let classes = this.props.detailReducer.current_page === number ? "page-item active" : 'page-item';
            return (
                <li key={number} className={classes} onClick={() => this.makeHttpRequestWithPage(number)}><a className="page-link">{number}</a></li>
            );
        });

        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-8 col-md-10 mx-auto">
                        {contents}
                    </div>
                </div>
                <div>
                    <div className="row bootstrap snippets" style={{ "margin": "auto" }}>
                        <div className="col-md-6 col-md-offset-2 col-sm-12">
                            <div className="comment-wrapper">
                                <div className="panel panel-info">

                                    <div className="panel-heading">
                                        <h1>Comment </h1>
                                    </div>

                                    <div className="panel-body">
                                        <form onSubmit={this.submitForm}>
                                            <textarea
                                                className="form-control"
                                                placeholder="write a comment..."
                                                name="commenttext"
                                                value={this.state.commenttext}
                                                onChange={this.handleOnchange}
                                                rows="3">
                                            </textarea>
                                            <br></br>
                                            <button type="submit" className="btn btn-info pull-right">Post</button>
                                        </form>
                                        <div className="clearfix"></div>
                                        <hr></hr>
                                        <ul className="media-list">
                                            {commentform}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="clearfix">
                    <nav aria-label="...">
                        <ul className="pagination">
                            <li className="page-item ">
                                <a className="page-link">Previous</a>
                            </li>
                            {renderPageNumbers}
                            <li className="page-item">
                                <a className="page-link">Next</a>
                            </li>
                        </ul>
                    </nav >
                </div >
            </React.Fragment>
        );
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchDetailBlogFunc: (id) => {
            dispatch(fetchDetailBlog(id));
        },
        fetchCommentPerPageFunc: (postid, numpage) => {
            dispatch(fetchCommentPerPage(postid, numpage))
        },
        postcmtFunc: (formdata) => {
            dispatch(postcmt(formdata))
        }
    };
};

const mapStateToProps = state => {
    return { detailReducer: state.detailReducer };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Detail))