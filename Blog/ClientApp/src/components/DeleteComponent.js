﻿import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

export class Delete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            flag: false
        }
        this.handleDelete = this.handleDelete.bind(this)
    }
    handleDelete() {
        console.log("PostId", this.props.postid);
        axios.put(`api/article/${this.props.postid}`)
            .then(res => {
                console.log("data1",res);
                console.log("data2",res.data);             
            })
        this.setState({
            flag: true
        })
        alert("Successful")
    }   
    render() {
        if (this.state.flag) {
            
            return <Redirect to="/" />
        }
        return (
            <div>
                <button onClick={this.handleDelete} > Delete </button>
                </div>
        )
    }
}