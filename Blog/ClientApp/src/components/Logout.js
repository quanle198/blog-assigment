﻿import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
import { hadLogin } from "./../actions"
import { connect } from 'react-redux'
class Logout extends Component {

    componentDidMount() {
        const token = localStorage.getItem("token");
        if (token) {
            this.props.hadLoginFunc(true);
        } else {
            this.props.hadLoginFunc(false);
        }
    }

    handleLogout = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('username');
        localStorage.removeItem('userid');
        localStorage.removeItem('role');
        this.props.history.push('/login');
    }

    render() {
        return (
            <div>
                {this.props.loginReducer.loggedIn ? (
                    <div>
                        <div>Do you want to logout?</div>
                        <button onClick={this.handleLogout}>Logout</button>
                    </div>
                ) : (
                        <div>

                        </div>
                    )}
            </div>
        );
    }
}
const mapDispatchToProps = dispatch => {
    return {
        hadLoginFunc: (status) => {
            dispatch(hadLogin(status))
        }
    };
};
const mapStateToProps = state => {
    return { loginReducer: state.loginReducer };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Logout))
