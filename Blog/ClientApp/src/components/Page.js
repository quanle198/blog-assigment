import React, { Component } from "react";
import { Layout } from "./Layout";
import Home   from "./Home";
import  Myblog  from "./Myblog";
import  Detail  from "./Detail";
import  Login  from "./Login";
import  Logout  from "./Logout";
import { NavMenu } from "./NavMenu";
import  Post  from "./Post";
import { Switch, Route } from "react-router-dom";
import "../Assets/css/clean-blog.min.css";
import "../Assets/vendor/fontawesome-free/css/all.min.css";
import "../Assets/vendor/bootstrap/css/bootstrap.min.css";

export default class App extends Component {
    render() {
        return (
            <Layout>
                <NavMenu key={'sdfdf'} />
                <Switch>
                    <Route exact path="/" render={() => <Home />} />
                    <Route exact path="/login" render={() => <Login />} />
                    <Route exact path="/logout" render={() => <Logout />} />
                    <Route exact path="/my-blog" component={Myblog} />
                    <Route exact path="/my-blog/:id" render={() => <Myblog />} />
                    <Route path="/Detail/:id" render={() => <Detail />} />    
                    <Route exact path="/Post" render={() => <Post />} />                   
                </Switch>
            </Layout>
        );
    }
}
