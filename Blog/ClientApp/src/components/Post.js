import React, { Component } from 'react';
import { post } from 'axios';
import CKEditor from "react-ckeditor-component";
import { Redirect } from 'react-router-dom';
import { hadLogin } from "./../actions"
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

 class Post extends Component {

    static displayName = Post.name;
    constructor(props) {
        super(props)

        const token = localStorage.getItem("token")

        if (token == null) {
            this.props.hadLoginFunc(false)
        }

        this.state = {
            title: "",
            intro: "",
            file: null,
            content: "",
            flag: false
        }

        this.onChange = this.onChange.bind(this);
        this.handleOnchange = this.handleOnchange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    handleOnchange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    //////ckeditor
    onChange(evt) {
        console.log("onChange fired with event info: ", evt);
        var newContent = evt.editor.getData();
        this.setState({
            content: newContent
        })
    }

    submitForm(event) {
        event.preventDefault()
        const url = 'api/article/';
        const formData = new FormData();
        formData.set('body', this.state.file);
        formData.set('Title', this.state.title);
        formData.set('Intro', this.state.intro);
        formData.set('Content', this.state.content);
        formData.set('Status', true);
        formData.set('UserId', localStorage.getItem("userid"));
        const config = {
            headers: {
                'content-type': 'application/json',
            },
        };
        post(url, formData, config);
        this.setState({
            flag: true
        })
        //const { title, intro, content } = this.state
    }
    // Process IMG
    setFile(e) {
        this.setState({ file: e.target.files[0] });
    }

     render() {
         if (this.props.loginReducer.loggedIn === false) {
            return <Redirect to="/login" />
        }
        console.log(this.state)
        if (this.state.flag) {
            alert("Successful")
        }
        return (
            <div className="Post-Blog" style={{ margin: "auto", width: "500px" }}>
                <form onSubmit={this.submitForm} >
                    <h1>Post Blog</h1>
                    <hr />
                    <div className="form-group">
                        <label>Title:</label>
                        <input
                            className="form-control"
                            name="title"
                            id="title"
                            value={this.state.title}
                            onChange={this.handleOnchange}
                        />
                    </div>

                    <div className="form-group">
                        <label >Intro:</label>
                        <input
                            type="text"
                            name="intro"
                            className="form-control"
                            value={this.state.intro}
                            id="intro"
                            onChange={this.handleOnchange}
                        />
                    </div>

                    <div className="form-group">
                        <label >Photo:</label>
                        <input type="file" onChange={e => this.setFile(e)} />
                    </div>

                    <div className="form-group">
                        <label >Content:</label>
                        <CKEditor
                            activeClass="p10"
                            content={this.state.content}
                            events={{
                                "change": this.onChange
                            }}
                        />
                    </div>

                    <button type="submit" className="btn btn-default">Submit</button>
                </form>
            </div>
        );
    }
}
const mapDispatchToProps = dispatch => {
    return {
        hadLoginFunc: (status) => {
            dispatch(hadLogin(status))
        }
    };
};
const mapStateToProps = state => {
    return { loginReducer: state.loginReducer };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Post))