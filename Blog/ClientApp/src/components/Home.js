import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Delete } from './DeleteComponent'
import { store } from "./../store";
import { fetchBlogPerPage } from "./../actions"
//import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
class Home extends Component {

    static displayName = Home.name;

    makeHttpRequestWithPage = async pageNumber => {
        this.props.fetchBlogPerPageFunc(pageNumber);
    }

    componentDidMount() {
        this.makeHttpRequestWithPage(1);
    }

    static renderArticlesTable(articles) {
        return (
            articles.map(article => {
                var options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };
                const date = new Date(article.createdAt).toLocaleDateString([], options);
                return (
                    <React.Fragment key={article.id}>
                        <div className="post-preview">
                            <Link to={`/Detail/${article.id}`}>
                                <h2 className="post-title">{article.title}</h2>
                                <img src={require(`../Assets/img/${article.pathImage}`)} alt="" width="300px" style={{ textAlign: "center" }} />
                                <h3 className="post-subtitle">
                                    {article.intro}
                                </h3>
                            </Link>
                            <p className="post-meta">
                                Posted by
                        <span style={{ "fontWeight": "bold" }}> {article.username} </span>
                                on
                                <span style={{ "fontWeight": "bold" }}> {date} </span>
                            </p>
                        </div>
                        {(localStorage.getItem("role") === "Administrator")
                            ?
                            <Delete postid={article.id} />
                            :
                            " "}
                        <hr></hr>

                    </React.Fragment>
                )
            })
        )
    }

    render() {
        console.log("reder", store.getState())
        let contents = this.props.homeReducer.loading
            ? <p><em>Loading...</em></p>
            : Home.renderArticlesTable(this.props.homeReducer.article)
        // Get number page
        const pageNumbers = [];
        if (this.props.homeReducer.total !== null) {
            for (let i = 1; i <= Math.ceil(this.props.homeReducer.total / this.props.homeReducer.per_page); i++) {
                pageNumbers.push(i);
            }
        }
        // Render Page Number
        let renderPageNumbers = pageNumbers.map(number => {
            let classes = this.props.homeReducer.current_page === number ? "page-item active" : 'page-item';
            return (
                <li key={number} className={classes} onClick={() => this.makeHttpRequestWithPage(number)}><a className="page-link">{number}</a></li>
            );
        });
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-8 col-md-10 mx-auto">
                        {contents}
                    </div>
                </div>
                <div className="clearfix">
                    <nav aria-label="...">
                        <ul className="pagination">
                            <li className="page-item ">
                                <a className="page-link">Previous</a>
                            </li>
                            {renderPageNumbers}
                            <li className="page-item">
                                <a className="page-link">Next</a>
                            </li>
                        </ul>
                    </nav >
                </div >
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => {
    return { homeReducer: state.homeReducer };
}
const mapDispatchToProps = dispatch => {
    return {
        fetchBlogPerPageFunc: (numpage) => {
            dispatch(fetchBlogPerPage(numpage));
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);