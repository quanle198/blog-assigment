import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { withRouter } from 'react-router-dom'
import { fetchMyblog, hadLogin } from "./../actions"
import { connect } from 'react-redux'

class Myblog extends Component {
    static displayName = Myblog.name;

    constructor(props) {
        super(props);
       
        const token = localStorage.getItem("token")

        if (token == null) {
            this.props.hadLoginFunc(false)
        }       
    }
    componentDidMount() {
        let id = localStorage.getItem("userid");
        this.props.fetchMyblogFunc(id);
    }
    static renderArticlesTable(articles) {
        let i = 1;
        return (
            <table className='table'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Intro</th>
                        <th>Content</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {articles.map(article => {
                        return (
                            <tr key={article.id}>
                                <td>{i++}</td>
                                <td><img src={require(`../Assets/img/${article.pathImage}`)} alt={`Baz taking a`} width="120px" /></td>
                                <td>{article.title}</td>
                                <td>{article.intro}</td>
                                <td><div>{article.content}</div></td>
                                <td>Edit</td>
                                <td>Delete</td>
                            </tr>)
                    }
                    )}
                </tbody>
            </table>
        );
    }

    render() {
        if (this.props.loginReducer.loggedIn === false) {
            return <Redirect to="/login" />
        }
        let contents = this.props.myblogReducer.loading
            ? <p><em>Loading...</em></p>
            : Myblog.renderArticlesTable(this.props.myblogReducer.article)
        return (
            <div>
                <h1>My Blog</h1>
                <p> </p>
                {contents}
            </div>
        );
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchMyblogFunc: (id) => {
            dispatch(fetchMyblog(id));
        },
        hadLoginFunc: (status) => {
            dispatch(hadLogin(status))
        }
    };
};

const mapStateToProps = state => {
    return {
        myblogReducer: state.myblogReducer,
        loginReducer: state.loginReducer
    };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Myblog))