import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import { fetchUser, hadLogin, Userchange, Passchange } from "./../actions"
//import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class Login extends Component {

    static displayName = Login.name;

    constructor(props) {
        super(props);

        this.props.hadLoginFunc(true);
        const token = localStorage.getItem("token")
        if (token === null) {
            this.props.hadLoginFunc(false);
        }

        this.state = {
            username: '',
            password: '',
        };
        this.handleOnchange = this.handleOnchange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    componentDidMount() {
        this.props.fetchUserFunc();
    }

    handleOnchange(event) {
        //this.setState({
        //    [event.target.name]: event.target.value
        //});
        if (event.target.name === 'username') {
            this.props.UserchangeFunc(event.target.value)
        }
        if (event.target.name === 'password') {
            this.props.PasschangeFunc(event.target.value)
        }
    }

    submitForm(event) {
        console.log("infostate", this.props.loginReducer)
        event.preventDefault()
        const { username, password } = this.props.loginReducer
        this.props.loginReducer.account.forEach((accounts) => {
            if (username === accounts.userName && password === accounts.password) {
                localStorage.setItem("token", "quanfxxxxxxxxxxxxxx")
                localStorage.setItem("userid", accounts.userId)
                localStorage.setItem("username", accounts.userName)
                localStorage.setItem("role", accounts.role)
                this.props.hadLoginFunc(true);
                console.log("infohadlogin", this.props.loginReducer)
                this.props.history.push('/')
            }
        })
    }

    render() {
        return (
            <div className="card-body" style={{ margin: "auto", width: "400px" }}>
                <h4 className="card-title text-center mb-4 mt-1">Sign in </h4>
                <p className="text-success text-center"></p>
                <form onSubmit={this.submitForm}>
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text"> <i className="fa fa-user"></i> </span>
                            </div>
                            <input
                                name="username"
                                className="form-control"
                                placeholder="UserName"
                                type="text"
                                value={this.props.loginReducer.username || ''}
                                onChange={this.handleOnchange}
                            />
                        </div>
                    </div>

                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text"> <i className="fa fa-lock"></i> </span>
                            </div>
                            <input
                                className="form-control"
                                name="password"
                                placeholder="******"
                                type="password"
                                value={this.props.loginReducer.password || ''}
                                onChange={this.handleOnchange}
                            />
                        </div>
                    </div>

                    <div className="form-group">
                        <button className="btn btn-primary btn-block"> Login  </button>
                    </div>
                    <p className="text-center"><a href="" className="btn">Forgot password?</a></p>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchUserFunc: () => {
            dispatch(fetchUser());
        },
        hadLoginFunc: (status) => {
            dispatch(hadLogin(status))
        },
        UserchangeFunc: (text) => {
            dispatch(Userchange(text))
        },
        PasschangeFunc: (textpass) => {
            dispatch(Passchange(textpass))
        },

    };
};

const mapStateToProps = state => {
    console.log(state.loginReducer.username);
    return {
        loginReducer: state.loginReducer,
    };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login))