import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import './NavMenu.css';

export class NavMenu extends Component {
    static displayName = NavMenu.name;

    constructor(props) {
        super(props);
        this.state = {
            collapsed: true,
            loggedIn: false
        };
    }

    render() {
        const token = localStorage.getItem("token");
        const user = localStorage.getItem("username");
        const userid = localStorage.getItem("userid");
        let loggedIn;
        if (token) {
            loggedIn = true;
        }
        // reder username is logged
        let username = user
            ? user
            : <p><em>Customer</em></p>
        return (
            <header>
                <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
                    <Container>
                        <NavbarBrand tag={Link} to="/">BlogFX</NavbarBrand>
                        <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                        <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
                            <ul className="navbar-nav flex-grow">
                                <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/">Home</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/post">Post</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} className="text-dark" to={`/my-blog/${userid}`}>My Blog</NavLink>
                                </NavItem>

                                <LoginComponent loggedIn={loggedIn}></LoginComponent>

                                <span style={{
                                    "marginTop": "-5px",
                                    "fontSize": "30px",
                                    "fontFamily": "cursive",
                                    "color": "darkgray"
                                }}>
                                    {username}
                                </span>
                            </ul>
                        </Collapse>
                    </Container>
                </Navbar>
            </header>
        );
    }
}

const LoginComponent = props => {
    console.log(props.loggedIn);
    if (props.loggedIn) {
        return (
            <NavItem>
                <NavLink
                    tag={Link}
                    className="text-dark"
                    to="/logout">
                    Logout
                </NavLink>
            </NavItem>
        );
    } else {
        return (
            <NavLink
                tag={Link}
                className="text-dark"
                to="/login">
                Login
            </NavLink>
        );
    }
};