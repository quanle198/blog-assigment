﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.EF
{
    public class Comment
    {
        public Comment()
        {
            this.CreateAt = DateTime.Now;
            this.Content = "aaa";
        }

        [Key]
        public int CommentId { get; set; }
        public DateTime CreateAt { get; set; }
        public string Content { get; set; }

        public int UserId { get; set; }
        //public virtual User User { get; set; }

        public int PostId { get; set; }      
        //public virtual Post Post { get; set; }

    }
}
