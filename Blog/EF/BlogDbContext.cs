﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.EF
{
    public class BlogDbContext : DbContext
    {
        public BlogDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Comment>()
            //.HasOne<User>(c => c.User);
            ////.WithMany(u => u.Comments)
            ////.HasForeignKey(c => c.UserId)
            ////.OnDelete(DeleteBehavior.Restrict);

            //modelBuilder.Entity<Comment>()
            //.HasOne<Post>(c => c.Post);
            ////.WithMany(p => p.Comments)
            ////.HasForeignKey(c => c.PostId)
            ////.OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasData(
                 new User { UserId = 1, UserName = "Admin", Password = "123", Role = "Administrator" },
                 new User { UserId = 2, UserName = "Quan", Password = "123", Role = "User" });

        }
    }
}
