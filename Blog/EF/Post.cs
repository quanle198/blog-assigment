﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.EF
{
    public class Post
    {
        [Key]
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Intro { get; set; }
        public string ImagePath { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Content { get; set; }
        public bool Status { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        //public ICollection<Comment> Comments { get; set; }

    }
}
