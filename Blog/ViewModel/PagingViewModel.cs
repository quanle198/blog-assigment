﻿using Blog.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.ViewModel
{
    public class PagingViewModel
    {
        public int CurrentPage { get; set; }
        public int TotalPage { get; set; }
        public int TotalItem { get; set; }
        public int Per_page { get; set; }
        public List <CreateViewModel> Data { get; set; }
    }
}
