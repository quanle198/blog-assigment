﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Blog.ViewModel
{
    public class CreateViewModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Intro { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Content { get; set; }
        public bool Status { get; set; }
        public IFormFile Photo { get; set; }
        public string PathImage { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public int CmtId { get; set; }
        public string ContentCmt { get; set; }
        public DateTime CreatedAtCmt { get; set; }

    }
}
