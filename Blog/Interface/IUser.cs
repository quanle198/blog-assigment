﻿using Blog.EF;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Interface
{
    public interface IUser
    {
        IEnumerable<User> GetUsers();
    }
}
