﻿using Blog.EF;
using Blog.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Interface
{
    public interface IComment
    {
        IEnumerable<Comment> GetComments();
        PagingViewModel GetCommentPerPage(int id, int postid);
        Task PostComment(Comment comment);
    }
}
