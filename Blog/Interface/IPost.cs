﻿using Blog.EF;
using Blog.ViewModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
namespace Blog.Interface
{
    public interface IPost 
    {
        IEnumerable<CreateViewModel> GetArticleItems();
        PagingViewModel GetArticlePerPage(int id);
        CreateViewModel GetArticleItem(int id);
        Task <List<CreateViewModel>> GetMyBlog(int id);
        void AddPost(Post item);
        Task SaveChange();
        int DeletePost( int id);
    }
}
