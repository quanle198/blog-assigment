﻿using Blog.EF;
using Blog.Interface;
using Blog.ViewModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TinifyAPI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Blog.Implementation
{
    public class Post_Implementation : IPost
    {
        private readonly BlogDbContext db;
        public Post_Implementation(BlogDbContext context)
        {
            db = context;
        }
        public IEnumerable<CreateViewModel> GetArticleItems()
        {
            var model = (from p in db.Posts
                         join u in db.Users on p.UserId equals u.UserId
                         where p.Status == true
                         select new CreateViewModel { ID = p.PostId, Title = p.Title, Intro = p.Intro, CreatedAt = p.CreatedAt, PathImage = p.ImagePath, Content = p.Content, UserId = u.UserId, Username = u.UserName })
                              .OrderByDescending(x => x.ID)
                              .ToList();
            return model;
        }
        public PagingViewModel GetArticlePerPage(int id)
        {
            int Per_page = 3;
            int TotalItem = ((from p in db.Posts where p.Status == true select p).Count());
            double num = TotalItem / (double)Per_page;
            int TotalPage = (int)Math.Ceiling(num);
            int Start = (id * Per_page - (Per_page - 1)) - 1;
            var model = (from p in db.Posts
                         join u in db.Users on p.UserId equals u.UserId
                         where p.Status == true
                         select new CreateViewModel { ID = p.PostId, Title = p.Title, Intro = p.Intro, CreatedAt = p.CreatedAt, PathImage = p.ImagePath, Content = p.Content, Username = u.UserName, Role = u.Role })
                              .OrderByDescending(x => x.ID).Skip(Start).Take(Per_page)
                              .ToList();
            PagingViewModel paging = new PagingViewModel()
            {
                CurrentPage = id,
                TotalPage = TotalPage,
                Per_page = Per_page,
                TotalItem = TotalItem,
                Data = model
            };
            return paging;
        }
        public CreateViewModel GetArticleItem(int id)
        {
            var model = (from p in db.Posts
                         join u in db.Users on p.UserId equals u.UserId
                         where p.PostId == id && p.Status == true
                         select new CreateViewModel { ID = p.PostId, Title = p.Title, Intro = p.Intro, CreatedAt = p.CreatedAt, PathImage = p.ImagePath, Content = p.Content, Status = p.Status, Username = u.UserName, UserId = u.UserId })
                              .SingleOrDefault();
            return model;
        }
        public void AddPost(Post item)
        {
            db.Posts.Add(item);
            db.SaveChanges();
        }
        public async Task<List<CreateViewModel>> GetMyBlog(int id)
        {
            var model = await (from p in db.Posts
                               join u in db.Users on p.UserId equals u.UserId
                               where u.UserId == id && p.Status == true
                               select new CreateViewModel { ID = p.PostId, Title = p.Title, Intro = p.Intro, CreatedAt = p.CreatedAt, PathImage = p.ImagePath, Content = p.Content, Username = u.UserName })
                              .ToListAsync();
            return model;
        }
        public async Task SaveChange()
        {
            await db.SaveChangesAsync();
        }
        public int DeletePost(int id)
        {
            Post item = new Post();
            item = (from p in db.Posts
                    where (p.PostId == id)
                    select p
                          ).SingleOrDefault();
            item.Status = false;
            if (item == null)
            { return 0; }
            db.Entry(item).State = EntityState.Modified;
            return 1;
        }
    }
}
