﻿using Blog.EF;
using Blog.Interface;
using Blog.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Implementation
{
    public class Comment_Implementation : IComment
    {
        private readonly BlogDbContext db;
        public Comment_Implementation(BlogDbContext context)
        {
            db = context;
        }
        public  IEnumerable<Comment> GetComments()
        {
            return  db.Comments.ToList();
        }
        public  PagingViewModel GetCommentPerPage(int id, int postid)
        {
            int Per_page = 5;
            int TotalItem = ((from c in db.Comments where c.PostId == postid select c).Count());
            double num = TotalItem / (double)Per_page;
            int TotalPage = (int)Math.Ceiling(num);
            int Start = (id * Per_page - (Per_page - 1)) - 1;
            var lstCmt =  (from c in db.Comments
                               join u in db.Users on c.UserId equals u.UserId
                               join p in db.Posts on c.PostId equals p.PostId
                               where p.PostId == postid
                               select new CreateViewModel { ID = p.PostId, CmtId = c.CommentId, Title = p.Title, Intro = p.Intro, CreatedAt = p.CreatedAt, PathImage = p.ImagePath, Content = p.Content, UserId = u.UserId, Username = u.UserName, ContentCmt = c.Content, CreatedAtCmt = c.CreateAt })
                          .OrderByDescending(x => x.CmtId).Skip(Start).Take(Per_page)
                          .ToList();
            PagingViewModel paging = new PagingViewModel()
            {
                CurrentPage = id,
                TotalPage = TotalPage,
                Per_page = Per_page,
                TotalItem = TotalItem,
                Data = lstCmt
            };
            return paging;
        }
        public async Task PostComment( Comment comment)
        {
            db.Comments.Add(comment);
            await db.SaveChangesAsync();
        }
    }
}
