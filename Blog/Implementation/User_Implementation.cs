﻿using Blog.EF;
using Blog.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Implementation
{
    public class User_Implementation : IUser
    {
        private readonly BlogDbContext db;
        public User_Implementation(BlogDbContext context)
        {
            db = context;
        }
        public  IEnumerable<User> GetUsers()
        {
            return db.Users.ToList();
        }
    }
}
